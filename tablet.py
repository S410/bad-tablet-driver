#!/usr/bin/env python3

import queue
import sys
import threading
import time
from abc import abstractmethod
from collections.abc import MutableSequence, Sequence
from dataclasses import dataclass
from enum import Enum
from queue import Queue
from typing import Any, Callable, Optional, Tuple

import evdev.ecodes as ec
import pyudev
import usb.core
import usb.util
from evdev import AbsInfo, UInput


class NoDevice(Exception):
    pass


class DeviceType(Enum):
    Tablet = 0
    Keyboard = 1


EvList = dict[tuple[int, int], ec]
EvMapping = dict[DeviceType, EvList]
ByteArray = MutableSequence[int]  # Actually array.array("B")


def log(*args: object, **kwargs: object) -> None:
    kwargs["file"] = sys.stderr
    print(*args, **kwargs)  # type: ignore


def log_evmap(evmap: EvMapping) -> None:
    for type, events in evmap.items():
        log(f"{type.name}:")
        for (t, c), value in events.items():
            str_name = ec.bytype[t][c]
            if isinstance(str_name, list):
                str_name = str_name[0]

            log(f"    {str_name}: {value}")


def go(f: Callable[..., Any], *args: Any, **kwargs: Any) -> None:
    t = threading.Thread(target=f, args=args, kwargs=kwargs)
    t.start()


class DevId:
    vendor: int
    vendor_str: str
    product: int
    product_str: str

    def __init__(self, vendor: int | str, product: int | str):
        def parse(value: str | int) -> int:
            if isinstance(value, str):
                value = int(value, 16)

            if isinstance(value, int):
                if value < 0 or value > 0xFFFF:
                    raise ValueError
            else:
                raise ValueError(f"Unsupported type: {type(value)}")

            return value

        vendor = parse(vendor)
        product = parse(product)

        self.vendor = vendor
        self.vendor_str = "%04x" % self.vendor
        self.product = product
        self.product_str = "%04x" % self.product

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, DevId):
            return (
                other.vendor == self.vendor and other.product == self.product
            )

        return False


class UsbReader:
    """
    UsbReader finds a USB device, feeds it init strings, and it for reading.
    """

    _dev: usb.core.Device
    _ep: usb.core.Endpoint

    def __init__(self, devid: DevId, init_strs: Sequence[int]) -> None:
        dev = usb.core.find(idVendor=devid.vendor, idProduct=devid.product)

        # Resetting the device also allows stealing it
        # from other instances of this script
        dev.reset()

        self._dev = dev
        if dev.is_kernel_driver_active(0):
            dev.detach_kernel_driver(0)
        self._ep = dev[0][(0, 0)][0]  # It works?

        for s in init_strs:
            usb.util.get_string(dev, s)

    def read(self) -> ByteArray:
        while True:
            try:
                msg: ByteArray = self._dev.read(
                    self._ep.bEndpointAddress,
                    self._ep.wMaxPacketSize,
                )

                return msg

            except usb.core.USBError as e:
                match e.errno:
                    case 19:
                        raise NoDevice("Unplugged")
                    case 5:
                        log("I/O Error")

                        # Hope it's a fluke
                        time.sleep(1.0)
                    case 110:
                        # Operation timedout
                        pass
                    case _:
                        raise e


UINPUT_ABS_RANGE = 1_000_000
FP_PLACES = 10_000


class MessageParser:
    # Pen state
    _hovering: bool

    # Device specific
    _x_to_abs_range: int = UINPUT_ABS_RANGE
    _y_to_abs_range: int = UINPUT_ABS_RANGE

    # Settings
    _x_offset: int
    _y_offset: int

    _x_scale: int
    _y_scale: int

    def __init__(self) -> None:
        self._hovering = False

        self._x_offset = 0
        self._y_offset = 0

        self._x_scale = FP_PLACES
        self._y_scale = FP_PLACES

    def scale_xy(self, x: int, y: int) -> Tuple[int, int]:
        """
        scale x and y in accordance with the settings and UInput ABS range
        """
        x = x * self._x_to_abs_range // FP_PLACES
        y = y * self._y_to_abs_range // FP_PLACES
        x = x * self._x_scale // FP_PLACES
        y = y * self._y_scale // FP_PLACES

        return x, y

    @abstractmethod
    def parse_message(self, event: ByteArray) -> EvMapping: ...


class UgeeParser(MessageParser):
    _buttons = [
        [ec.BTN_0],
        [ec.BTN_1],
        [ec.BTN_2],
        [ec.BTN_3],
        [ec.BTN_4],
        [ec.BTN_5],
        [ec.BTN_6],
        [ec.BTN_7],
    ]

    _x_to_abs_range: int = UINPUT_ABS_RANGE * FP_PLACES // 40_000
    _y_to_abs_range: int = UINPUT_ABS_RANGE * FP_PLACES // 24_000

    def __init__(self) -> None:
        super().__init__()

        self._x_offset = 0
        self._y_offset = 0

        # Cut off right third of the screen (2560+1280)
        self._x_scale = 6_666
        # Adjust y to get proper aspect ratio (5/3 -> 16/9)
        self._y_scale = 10_666

    def _parse_movement(self, event: ByteArray) -> EvList:
        x = (event[3] << 8) | event[2]
        y = (event[5] << 8) | event[4]

        x, y = self.scale_xy(x, y)

        return {
            (ec.EV_ABS, ec.ABS_X): x,
            (ec.EV_ABS, ec.ABS_Y): y,
            (ec.EV_ABS, ec.ABS_PRESSURE): (event[7] << 8) | event[6],
            (ec.EV_ABS, ec.ABS_DISTANCE): (event[1] & 0b001) == 0,
            (ec.EV_KEY, ec.BTN_STYLUS): (event[1] & 0b010) != 0,
            (ec.EV_KEY, ec.BTN_STYLUS2): (event[1] & 0b100) != 0,
        }

    def _parse_buttons(self, event: ByteArray) -> EvList:
        evlist = {}

        byte = event[4]
        for i, btns in enumerate(self._buttons):
            is_pressed = (byte & (1 << i)) != 0

            for btn in btns:
                evlist[(ec.EV_KEY, btn)] = is_pressed

        return evlist

    def parse_message(self, event: ByteArray) -> EvMapping:
        if event[0] != 0x07:
            log(
                "Unknown event type!\n"
                f"{' '.join(f'{x:02X}' for x in event)}",
            )
            return {}

        match event[1]:
            # 0x8*: Movement in the tracking area
            #       Last 3 bits indicate which buttons are pressed
            # 0xC0: Pen left the tracking area
            case t if (t & 0b1111_1000) == 0x80 or t == 0xC0:
                evlist = self._parse_movement(event)

                active = t != 0xC0
                if self._hovering != active:
                    self._hovering = active

                    evlist |= {
                        (ec.EV_KEY, ec.BTN_TOOL_PEN): active,
                    }

                return {DeviceType.Tablet: evlist}

            # Buttons
            case 0xE0:
                return {DeviceType.Keyboard: self._parse_buttons(event)}

            # Unknown
            case _:
                log(
                    "Unknown event subtype!\n"
                    f"{' '.join(f'{x:02X}' for x in event)}",
                )
                return {}


class UsbDriver:
    devid: DevId

    @abstractmethod
    def drive(self) -> None: ...


class UgeeTablet(UsbDriver):
    devid = DevId(0x5543, 0x0081)
    init_strings = (100, 123)
    parser: MessageParser = UgeeParser()

    events: Queue[EvMapping]

    def __init__(self, events: Queue[EvMapping]) -> None:
        log("New UgeeTablet")

        self.events = events

    def drive(self) -> None:
        try:
            usb_device = UsbReader(self.devid, self.init_strings)
        except NoDevice:
            log(f"Can't drive nonexistent device")
            return

        ident = threading.get_ident()

        log(f"Driving device [thr.{ident}]")

        try:
            while True:
                msg = usb_device.read()
                evmap = self.parser.parse_message(msg)
                self.events.put(evmap)
        except NoDevice:
            log(f"Device unplugged [thr.{ident}]")
            return
        except Exception as e:
            log(f"Exception in [thr.{ident}]", e, sep="\n")


def monitor_udev(supported: Sequence[UsbDriver]) -> None:
    context = pyudev.Context()
    monitor = pyudev.Monitor.from_netlink(context)
    monitor.filter_by(subsystem="usb")

    for action, device in monitor:
        if action == "add":
            try:
                devid = DevId(
                    device.get("ID_VENDOR_ID"),
                    device.get("ID_MODEL_ID"),
                )
            except ValueError:
                continue

            for drv in supported:
                if drv.devid == devid:
                    log(
                        "Matched a supported device in Udev. "
                        "Starting a driver thread..."
                    )
                    go(drv.drive)


def uinput(events: Queue[EvMapping]) -> None:
    virt_tablet = UInput(
        events={
            ec.EV_KEY: [
                ec.BTN_TOOL_PEN,
                ec.BTN_STYLUS,
                ec.BTN_STYLUS2,
            ],
            ec.EV_ABS: [
                (
                    ec.ABS_X,
                    AbsInfo(0, 0, UINPUT_ABS_RANGE, 0, 0, UINPUT_ABS_RANGE),
                ),
                (
                    ec.ABS_Y,
                    AbsInfo(0, 0, UINPUT_ABS_RANGE, 0, 0, UINPUT_ABS_RANGE),
                ),
                (ec.ABS_PRESSURE, AbsInfo(0, 0, 2048, 0, 0, 0)),
                (ec.ABS_DISTANCE, AbsInfo(0, 0, 2048, 0, 0, 0)),
            ],
        },
        input_props=[ec.INPUT_PROP_DIRECT],
        name="Virtual Tablet",
    )

    virt_keyboard = UInput(
        events={ec.EV_KEY: ec.bytype[ec.EV_KEY]},
        name="Virtual Buttons",
    )

    virt_devices = {
        DeviceType.Tablet: virt_tablet,
        DeviceType.Keyboard: virt_keyboard,
    }

    def write_evlist(virt: UInput, evlist: EvList) -> None:
        for t, v in evlist.items():
            virt.write(*t, v)

        virt.syn()

    def write_evmap(evmap: EvMapping) -> None:
        for devtype, evlist in evmap.items():
            write_evlist(virt_devices[devtype], evlist)

    while True:
        evmap = events.get()
        write_evmap(evmap)
    pass


if __name__ == "__main__":
    input_events: Queue[EvMapping] = Queue()

    supported = [
        UgeeTablet(input_events),
    ]

    go(uinput, input_events)
    go(monitor_udev, supported)

    for d in supported:
        go(d.drive)
