> "My fucking piece of shit tablet Ugee M708 aka XP-Pen Star 03 aka Parblo A610
> aka UC-Logic TABLET 1060 aka, I SHIT YOU NOT, **UC-LOIC** 1060 aka 5543:0081,
> speking in USB IDs, is a piece of shit and refuses to work under wayland even
> tho it's detected and even shows up in libwacom debug doohickey, so I wrote
> this piece of shit and now i want die"
>
> \- Me, 2021

While the tablet does (somewhat) work with `hid_uclogic` kernel module, it
doesn't seem to be recognized as a tablet by anything. No configuration options
seem to be exposed, and without the ability to map the working area to cover the
_correct_ screen, as well as scale it to the correct aspect ratio, using this
piece of work is hardly possible.

`digimend` drivers somewhat sorta worked, finally making the thing show up in
the settings and even be configurable! ...for a whole one day. After that one
miraculous event, everything went back to the way it was before. That is, the
drivers seemed to only feed the init strings to the device and give up on it
right after.

`OpenTabletDriver` ... "worked". Not at first. Back in 2021 it didn't even
detect the darn thing, but this year it did! On every 10th attempt or so. While,
at the same time, shitting into the logs that multiple devices were detected.
Does it work with the `input` subsystem or something, instead of grabbing the
usb device?

Oh, and on Windows things weren't much better: while the software does detect
the tablet and even allows to configure it (wow!), the drivers simply crash
every 10 minutes or so, require physically re-plugging the device. Ugh.

Anyway. Behold!

# 🚀 tablet.py 🚀

### ♻️ A Garbage Driver for a Garbage Tablet! ♻️

#### Features:

-   🚀 **Blazing Slow Python Code!**
-   🔓 **Totally Secure Code Audited By Literally Nobody!**
-   🥬 **Doesn't Require Root Permissions to Run** <sub>(but _does_ require them to actually work ¯\\\_(ツ)\_/¯)</sub>
-   ⚙️ **Totally doesn't have my Personal Preferences hardcoded in the thing!**

# 🚮 Installation 🚮

(Assuming Arch)

```sh
# Install dependencies
pacman -S python-evdev python-pyudev python-pyusb

# Copy files to all the right places
cp tablet.service tablet-restart-after-suspend.service /etc/systemd/system
cp tablet.py /usr/local/bin

# Enable the services
systemctl enable --now tablet
systemctl enable tablet-restart-after-suspend
```

No idea why anyone would want this, though.
